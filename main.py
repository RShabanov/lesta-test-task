import time
from regex import D


def is_even_div(value):
    return value % 2 == 0


def is_even_bit(value):
    return value & 1 == 0


if __name__ == "__main__":   
    
    for i in range(10):
        assert is_even_bit(i) == is_even_div(i)
        