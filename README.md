## Тестовое задание
Задания находятся в отдельных ветках: 
<a href="https://gitlab.com/RShabanov/lesta-test-task/-/tree/first-task">first-task</a> | <a href="https://gitlab.com/RShabanov/lesta-test-task/-/tree/second-task">second-task</a> | <a href="https://gitlab.com/RShabanov/lesta-test-task/-/tree/third-task">third-task</a>

Возможны два варианта определения четности числа: с помощью деления по модулю на два и проверки первого бита.

### Деление по модулю на 2
**Плюсы**:
* простота и понятность.

**Минусы**:
* может работать медленнее, в зависимости от реализации.
---
### Проверка бита (логическое И)
Плюсы:
* может работать быстрее, если ограничиться проверкой `number & 1`.

Минусы:
* может быть не таким очевидным. 